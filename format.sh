#!/usr/bin/env bash

disk=nvme0n1

# partitioning
parted /dev/${disk} -- mklabel gpt
parted /dev/${disk} -- mkpart primary 512MiB 100%
parted /dev/${disk} -- mkpart ESP fat32 1MiB 512MiB
parted /dev/${disk} -- set 2 esp on

# formatting
mkfs.ext4 -L nixos /dev/${disk}p1
mkfs.fat -F 32 -n BOOT /dev/${disk}p2
