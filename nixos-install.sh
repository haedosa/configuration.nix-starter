#!/usr/bin/env bash
# run as the superuser root

mount /dev/disk/by-label/nixos /mnt
mkdir -p /mnt/boot
mount /dev/disk/by-label/BOOT /mnt/boot
# swapon /dev/disk/by-label/swap
nixos-generate-config --root /mnt
cp configuration.nix /mnt/etc/nixos/configuration.nix
umask 077
mkdir -p /mnt/root/.wireguard-keys
nix-shell -p wireguard --command 'wg genkey > /mnt/root/.wireguard-keys/private'
nix-shell -p wireguard --command 'wg pubkey < /mnt/root/.wireguard-keys/private > /mnt/root/.wireguard-keys/public'
nixos-install
